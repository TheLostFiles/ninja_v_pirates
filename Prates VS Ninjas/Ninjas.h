#pragma once
#include "Character.h"


class Ninjas : public Character //class 
{
public: // public
	Ninjas(); // method of create a ninja
	void ThrowStars(); // method gor throwing stars
	int Attack() override; // method for attack
	void Help() override; // help override
	
private: // private
};

