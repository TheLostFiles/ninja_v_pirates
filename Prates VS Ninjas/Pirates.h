#pragma once
#include "Character.h"


class Pirates : public Character // class
{
public:
	Pirates(); // method for making pirates
	void UseSword(); // method for using sword
	int Attack() override; // method for attacking amount
	void Help() override; // help override
	
private: // private
};

