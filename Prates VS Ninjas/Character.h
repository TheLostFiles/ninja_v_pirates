#pragma once
#include "GameStructure.h"
#include <string>
using namespace std;


class Character : public GameStructure // class
{
public: // public
	string Name; // sets up a string for the name
	
	int GetHealth(); // method to get health
	void SetHealth(int health); // method that sets the health
	
	void DisplayCharacterStats(); // method that displays the character stats
	
	void Talk(string stuffToSay); // method that sets up talking
	void Talk(string stuffToSay, string name); // more complex method for talking

	void Story(); // method that gives the characters their story 
	
	virtual int Attack(); // int of the number for attack
	
	void Help() override; // help
	
private: // private 
	int Health; // int for health 
};

