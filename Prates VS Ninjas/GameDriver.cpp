#include "GameDriver.h"
#include <iostream>
#include <string>
#include <ctime>
using namespace std;


void GameDriver::DisplayInto() // method for displaying the intro
{
	cout << "Welcome to The character generator\n";
	cout << "Today we are going to get you started on making either a pirate or a ninja.\n\n";
}

void GameDriver::DisplayCharacterChoices() // method that displays the players choice on different characters
{
	cout << "Would you like to be a Pirate or a Ninja\n"; 
	cout << "1 - Ninja\n";
	cout << "2 - Pirate\n";
}

int GameDriver::RandomRollHealth() // methof for random health
{
	int randHealth; // int
	srand(time(0)); // setting the seed on to a chunk of time

	randHealth = rand() % 100 + 1; // random between 0-101
	
	return randHealth;// return the int
}

string GameDriver::Choose(string choice) // method for picking characters
{
	getline(cin, choice); // get line

	while (choice != "1" && choice != "2") // while loop checking if they put in a wrong number
	{
		cout << " You can't type that. Try again\n";
		DisplayCharacterChoices(); // reshows the players choice
		getline(cin, choice); // gets the line again
	}
	return choice; // returns choice
}




            



