#pragma once
#include <string>


class GameDriver // class
{
public: // public
	void DisplayInto(); // method for displaying the intro
	void DisplayCharacterChoices(); // method for displaying the choices you have
	
	int RandomRollHealth(); // method fir making random health

	std::string Choose(std::string choice); // string for choosing

private: // private
	
};

