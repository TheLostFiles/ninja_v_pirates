#include "Ninjas.h"
#include <iostream>
using namespace std;


Ninjas::Ninjas() // method for creating a ninja
{
	cout << "There is now a ninja\n";
}

void Ninjas::ThrowStars() // method for throwing stars
{
	cout << "YEET";
}

int Ninjas::Attack() // attack override
{
	return 25; // returns 25
}

void Ninjas::Help() // help
{
	cout << "this is ninja help";
}
