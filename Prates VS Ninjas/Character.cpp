#include "Character.h"
#include <iostream>
#include <ctime>


int Character::GetHealth() // gets the characters health
{
	return Health; // returns health
}

void Character::SetHealth(int health) // sets the characters health
{
	Health = health; // makes Health = health
}

void Character::DisplayCharacterStats() // method to display character stats 
{
	cout << "This character is named " << Name <<" and has "<< GetHealth() << " health\n";
	cout << "They can do " << Attack() << " attack damage against their enemies\n";
}

void Character::Talk(string stuffToSay) // method for talking
{
	cout << stuffToSay;
}

void Character::Talk(string stuffToSay, string name) // more advanced method for talking
{
	
	cout << name << stuffToSay;
}

void Character::Story() // method for randomly picking the story of the character
{
	int randText; // int
	srand(time(0)); // sets the seed to time

	randText = rand() % 3 + 1; // random number between 1-3

	if (randText == 1) // checks the number
	{
		cout << "He/She was a revered fighter, and always scared off anyone that thought that they could fight them\n";
	}
	if (randText == 2) // checks the number
	{
		cout << "He/She was abandoned as a child and was found by their leader which trained them and brought them up into their adult hood \n";
	}
	if (randText == 3) // checks the number
	{
		cout << "He/She was given a rare amulet that gave them some very special powers and made them string in any way they wanted\n";
	}
}

int Character::Attack() // method for attack amount
{
	return 10; // return 10
}

void Character::Help() //help
{
	cout << "this is the help";
}
